import pandas as pd
import numpy as np


def read_data(path):
	return pd.read_csv(path)


def summarize(data, name='Unknown data'):
	nrows, ncols = data.shape
	print()
	print('Analyzing', name, '...')
	print(data.info())
	print(name, 'has {} rows and {} cols'.format(nrows, ncols))
	print()
	print('Sample', name, ':')
	print(data.head())
	print()
	print('Generating', name, 'statistics...')
	print()
	print(data.describe())
	print('Done')


def split_data(data, label_index=None, split=0.8):
	msk = np.random.rand(len(data)) < split
	train = data[msk]
	dev = data[~msk]
	if label_index is not None:
		trainy = train.iloc[:, label_index]
		trainx = pd.concat(
		    [train.iloc[:, 0:label_index], train.iloc[:, label_index + 1:]], axis=1)
		devy = dev.iloc[:, label_index]
		devx = pd.concat(
		    [dev.iloc[:, 0:label_index], dev.iloc[:, label_index + 1:]], axis=1)
	return trainx, trainy, devx, devy


def create_chunks(trainX, trainY, num):
	global Xchunks
	global Ychunks
	Xchunks = [trainX[i:i + num] for i in range(0, len(trainX), num)]
	Ychunks = [trainY[i:i + num] for i in range(0, len(trainY), num)]


def next_batch(trainX, trainY, num, step):
	# inds = np.random.randint(0, len(trainX), num)
	# return trainX[inds], trainY[inds]
	ind = step % int(len(trainX) / num)
	return Xchunks[ind], Ychunks[ind]
	# start = step % len(trainX)
	# end = start + num
	# if end > len(trainX):
	# 	return np.concatenate(trainX[:end - len(trainX)],
	# 	                      trainX[start:]), np.concatenate(
	# 	                          trainY[:end - len(trainY)], trainY[start:])
	# else:
	# 	return trainX[start:end], trainY[start:end]