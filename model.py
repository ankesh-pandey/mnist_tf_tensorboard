import argparse
import os
import sys

import tensorflow as tf
from data_helpers import read_data
from data_helpers import summarize
from data_helpers import split_data
from data_helpers import next_batch

FLAGS = None


def train():
	# Import data

	print('Loading data...')
	train_data = read_data(FLAGS.data_dir + '/train.csv')
	test_data = read_data(FLAGS.data_dir + '/test.csv')
	print('Done')

	# Data analysis
	summarize(train_data, name='Training data')
	summarize(test_data, name='Test data')

	# Data preprocessing
	trainX, trainY, devX, devY = split_data(train_data, label_index=0)
	trainX, trainY, devX, devY = trainX.to_numpy(), trainY.to_numpy(
	), devX.to_numpy(), devY.to_numpy()
	trainX = trainX
	devX = devX
	test_data = test_data.to_numpy() / 255.0

	trainX, devX, test_data = trainX / 255.0, devX / 255.0, test_data / 255.0

	gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.33)
	sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))

	with tf.name_scope('input'):
		x = tf.placeholder(tf.float32, [None, 784], name='x-input')
		y_ = tf.placeholder(tf.int64, [None], name='y-input')

	def weight_variable(shape):
		initial = tf.truncated_normal(shape, stddev=0.1)
		return tf.Variable(initial)

	def bias_variable(shape):
		initial = tf.constant(0.1, shape=shape)
		return tf.Variable(initial)

	def variable_summaries(var):
		with tf.name_scope('summaries'):
			mean = tf.reduce_mean(var)
			tf.summary.scalar('mean', mean)
			with tf.name_scope('stddev'):
				stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
			tf.summary.scalar('stddev', stddev)
			tf.summary.scalar('max', tf.reduce_max(var))
			tf.summary.scalar('min', tf.reduce_min(var))
			tf.summary.histogram('histogram', var)

	def nn_layer(input_tensor, input_dim, output_dim, layer_name, act=tf.nn.relu):
		with tf.name_scope(layer_name):
			with tf.name_scope('weights'):
				weights = weight_variable([input_dim, output_dim])
				variable_summaries(weights)
			with tf.name_scope('biases'):
				biases = bias_variable([output_dim])
				variable_summaries(biases)
			with tf.name_scope('Wx_plus_b'):
				preactivate = tf.matmul(input_tensor, weights) + biases
				tf.summary.histogram('pre_activations', preactivate)
			activations = act(preactivate, name='activation')
			tf.summary.histogram('activations', activations)
			return activations

	hidden1 = nn_layer(x, 784, 512, 'layer1')

	with tf.name_scope('dropout'):
		keep_prob = tf.placeholder(tf.float32)
		tf.summary.scalar('dropout_keep_probability', keep_prob)
		dropped1 = tf.nn.dropout(hidden1, keep_prob)

	hidden2 = nn_layer(dropped1, 512, 128, 'layer2')

	with tf.name_scope('dropout'):
		dropped2 = tf.nn.dropout(hidden2, keep_prob)

	y = nn_layer(dropped2, 128, 10, 'layer3', act=tf.identity)

	with tf.name_scope('cross_entropy'):
		with tf.name_scope('total'):
			cross_entropy = tf.losses.sparse_softmax_cross_entropy(labels=y_, logits=y)
			tf.summary.scalar('cross_entropy', cross_entropy)

	with tf.name_scope('train'):
		train_step = tf.train.AdamOptimizer(
		    FLAGS.learning_rate).minimize(cross_entropy)

	with tf.name_scope('accuracy'):
		with tf.name_scope('correct_prediction'):
			correct_prediction = tf.equal(tf.argmax(y, 1), y_)
		with tf.name_scope('accuracy'):
			accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
	tf.summary.scalar('accuracy', accuracy)

	merged = tf.summary.merge_all()
	train_writer = tf.summary.FileWriter(FLAGS.log_dir + '/train', sess.graph)
	test_writer = tf.summary.FileWriter(FLAGS.log_dir + '/test')
	tf.global_variables_initializer().run()

	def feed_dict(train):
		if train:
			xs, ys = next_batch(trainX, trainY, 100)
			k = FLAGS.dropout
		else:
			xs, ys = devX, devY
			k = 1.0
		return {x: xs, y_: ys, keep_prob: k}

	for i in range(FLAGS.max_steps):
		if i % 10 == 0:
			summary, acc = sess.run([merged, accuracy], feed_dict=feed_dict(False))
			test_writer.add_summary(summary, i)
			print('Accuracy at step %s: %s' % (i, acc))
		else:
			if i % 100 == 99:
				run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
				run_metadata = tf.RunMetadata()
				summary, _ = sess.run(
				    [merged, train_step],
				    feed_dict=feed_dict(True),
				    options=run_options,
				    run_metadata=run_metadata)
				train_writer.add_run_metadata(run_metadata, 'step%03d' % i)
				train_writer.add_summary(summary, i)
				print('Adding run metadata for', i)
			else:
				summary, _ = sess.run([merged, train_step], feed_dict=feed_dict(True))
				train_writer.add_summary(summary, i)
	train_writer.close()
	test_writer.close()


def main(_):
	if tf.gfile.Exists(FLAGS.log_dir):
		tf.gfile.DeleteRecursively(FLAGS.log_dir)
	tf.gfile.MakeDirs(FLAGS.log_dir)
	with tf.Graph().as_default():
		train()


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument(
	    '--max_steps',
	    type=int,
	    default=1000,
	    help='Number of steps to run trainer.')
	parser.add_argument(
	    '--learning_rate',
	    type=float,
	    default=0.001,
	    help='Initial learning rate')
	parser.add_argument(
	    '--dropout',
	    type=float,
	    default=0.9,
	    help='Keep probability for training dropout.')
	parser.add_argument(
	    '--data_dir',
	    type=str,
	    default='/tmp',
	    help='Directory for storing input data')
	parser.add_argument(
	    '--log_dir', type=str, default='/tmp', help='Summaries log directory')

	FLAGS, unparsed = parser.parse_known_args()
	tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)